FROM openjdk:10-slim

COPY gradle/ /gradle/
COPY src/ /src/
COPY settings.gradle settings.gradle
COPY build.gradle build.gradle
COPY gradlew gradlew

# install git for semver plugin
RUN apt-get update && apt-get install -y git

# execute gradlew to download and cache gradle-distribution and java-packages and to compile sources
RUN ./gradlew assemble compileTestJava

ENTRYPOINT ["./gradlew","build", "--info"]
