#!/bin/bash

# Original shell script see https://ypereirareis.github.io/blog/2015/05/04/docker-with-shell-script-or-makefile/

# Output colors
COLOR_NORMAL="\\033[0;39m"
COLOR_RED="\\033[1;31m"
COLOR_BLUE="\\033[1;34m"

DOCKER_REGISTRY=docker-hub.informatik.haw-hamburg.de
PROJECT=adia
SERVICE_NAME=integration
VERSION=latest

DEPLOYMENTS_SVC=./deploy/services
DEPLOYMENTS_TESTS=./deploy/tests

NAMESPACE=$PROJECT-$SERVICE_NAME

log() {
  echo -e -n "$COLOR_BLUE"
  echo $1
  echo -e -n "$COLOR_NORMAL"
}

error() {
  echo -e -n "$COLOR_RED"
  echo ERROR - $1
  echo -e -n "$COLOR_NORMAL"
}

buildTests() {
  docker build -t $DOCKER_REGISTRY/$PROJECT/$SERVICE_NAME:$VERSION .
  [ $? != 0 ] && \
    error "Docker build failed!" && exit 100

  docker push $DOCKER_REGISTRY/$PROJECT/$SERVICE_NAME:$VERSION
  [ $? != 0 ] && \
    error "Docker push failed!" && exit 100
}

deploy() {
  log "Deploying all images (services and integrationtests)."
  kubectl -n $NAMESPACE create -f $DEPLOYMENTS_SVC -f $DEPLOYMENTS_TESTS
}

undeploy() {
  log "Deleting all deployments (services and integrationtests)."
  kubectl -n $NAMESPACE delete -f $DEPLOYMENTS_SVC -f $DEPLOYMENTS_TESTS
}

test() {
  log "Executing integration tests. See logs for details about current status."
  log "Make sure, services have been deployed!"
  kubectl -n $NAMESPACE delete -f $DEPLOYMENTS_TESTS
  kubectl -n $NAMESPACE create -f $DEPLOYMENTS_TESTS
}

logs() {
  log "Getting current logs from integrationtest-Pod."
  kubectl -n $NAMESPACE logs $SERVICE_NAME
}

info() {
  log "Getting deployment-infos."
  kubectl -n $NAMESPACE get po,svc
}

k() {
  kubectl -n $NAMESPACE $1 $2 $3 $4 $5 $6
}

help() {
  echo "-----------------------------------------------------------------------"
  echo "                      Available commands                              -"
  echo "-----------------------------------------------------------------------"
  echo -e -n "$COLOR_BLUE"
  echo "   > buildTests - To build and push the Docker image for integrationtests."
  echo "   > deploy - To deploy all images (services and integrationtests)."
  echo "   > undeploy - To delete all deployments (services and integrationtests)."
  echo "   > test - To start executing integration tests (integrationtest-Pod will be re-deployded)."
  echo "   > logs - To get current logs from the integrationtest-Pod."
  echo "   > info - To get info about our deployments."
  echo "   > k - execute kubectl with args."
  echo -e -n "$COLOR_NORMAL"
  echo "-----------------------------------------------------------------------"
}

$*
