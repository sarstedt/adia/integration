#!/usr/bin/env bash

# ----------------------------------------------------------------------------------------------------
# Author: Stefan Sarstedt
#
# this script
#   - fetches the latest versions of the provided artifacts (second commandline parameter) from nexus
#   - gets the artifacts from nexus
#   - copies the artifacts (jars) to an OUTPUT_DIR
#   - starts the jar
# ----------------------------------------------------------------------------------------------------

if [ "$#" -ne 3 ]; then
    echo "usage: ./`basename "$0"` <groupIp> <artifacts in format \"artifact1 artifact2 ...\"> <ports in format \"port_for_artifact1 port_for_artifact2 ...\">"
    echo "example: ./`basename "$0"` com.adia.srs \"bookingservice portservice\" \"8080 8081\""
    exit 1
fi

ARTIFACTS=($2)
PORTS=($3)
GROUP_ID=$1
OUTPUT_DIR=./build/servicejars

# this only works with Nexu repositories of version 3 (and above, if there are no API changes: https://help.sonatype.com/repomanager3/rest-and-integration-api)
NEXUS_REPOSITORY="https://srs.ful.informatik.haw-hamburg.de"

getLatestTagFromNexus() {
    echo "Searching for latest tag of group \"$1\" / artifact \"$2\" on Nexus-Repository \"$NEXUS_REPOSITORY\"."
    LATEST_TAG=`curl -X GET --silent --fail "$NEXUS_REPOSITORY/service/rest/v1/search/assets?maven.groupId=$1&maven.artifactId=$2" -H "accept: application/json" | grep "path" | xargs -0 | sed "s/.*$2\/\([^/]*\).*/\1/" | sort -u -r | head -1`

    if [ "$LATEST_TAG" == "" ]; then
        echo "No tag found for group \"$1\" / artifact \"$2\" on Nexus-Repository \"$NEXUS_REPOSITORY\". Aborting."
        exit 1
    fi

    echo "Latest tag of group \"$1\" / artifact \"$2\" is: \"$LATEST_TAG\"."
}

for i in "${!ARTIFACTS[@]}"
do
    ARTIFACT_ID="${ARTIFACTS[i]}"
    SERVER_PORT="${PORTS[i]}"

    getLatestTagFromNexus $GROUP_ID $ARTIFACT_ID

    ASSET="$GROUP_ID:$ARTIFACT_ID:$LATEST_TAG"
    echo "Getting \"$ASSET\" from \"$NEXUS_REPOSITORY\"."
    mvn org.apache.maven.plugins:maven-dependency-plugin:3.1.1:get -q -DremoteRepositories=https://srs.ful.informatik.haw-hamburg.de/repository/maven-snapshots/ -Dartifact=$ASSET
    if [ $? -ne 0 ]; then
        echo "Error getting asset \"$ASSET\" from \"$NEXUS_REPOSITORY\"."
        exit 1
    fi
    echo "Copying \"$ASSET\" to \"$OUTPUT_DIR\"."
    mvn org.apache.maven.plugins:maven-dependency-plugin:3.1.1:copy -q -Dartifact=$ASSET -DoutputDirectory=$OUTPUT_DIR
    if [ $? -ne 0 ]; then
        echo "Error copying asset \"$ASSET\" to \"$OUTPUT_DIR\"."
        exit 1
    fi

    JAR_FILE="$OUTPUT_DIR/$ARTIFACT_ID-$LATEST_TAG.jar"
    echo "Starting \"$JAR_FILE\" on port $SERVER_PORT in the background."
    java -Dserver.port=$SERVER_PORT -jar $JAR_FILE &> spring_$ARTIFACT_ID.log &
    if [ $? -ne 0 ]; then
        echo "Error starting \"$JAR_FILE\" on port $SERVER_PORT."
        exit 1
    fi
done
