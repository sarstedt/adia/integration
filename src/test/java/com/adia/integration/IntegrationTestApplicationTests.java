package com.adia.integration;

import io.restassured.RestAssured;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.client.HttpComponentsClientHttpRequestFactory;
import org.springframework.retry.backoff.FixedBackOffPolicy;
import org.springframework.retry.policy.SimpleRetryPolicy;
import org.springframework.retry.support.RetryTemplate;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.web.client.RestTemplate;

import static io.restassured.RestAssured.when;
import static org.hamcrest.Matchers.hasItems;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = IntegrationTestApplication.class)
public class IntegrationTestApplicationTests {

    private Log log = LogFactory.getLog(getClass());

    @Value("${BOOKINGSERVICE_BASE_URL:http://localhost:8080}")
    private String bookingserviceBaseURL;

    @Value("${SERVICE_REACHABILITY_BACK_OFF_PERIOD:3000}")
    private long SERVICE_REACHABILITY_BACK_OFF_PERIOD;
    @Value("${SERVICE_REACHABILITY_MAX_ATTEMPTS:50}")
    private int SERVICE_REACHABILITY_MAX_ATTEMPTS;

    private void checkForReachabilityOfServices() {
        RetryTemplate retryTemplate = new RetryTemplate();
        FixedBackOffPolicy fixedBackOffPolicy = new FixedBackOffPolicy();
        fixedBackOffPolicy.setBackOffPeriod(SERVICE_REACHABILITY_BACK_OFF_PERIOD);
        retryTemplate.setBackOffPolicy(fixedBackOffPolicy);
        SimpleRetryPolicy retryPolicy = new SimpleRetryPolicy();
        retryPolicy.setMaxAttempts(SERVICE_REACHABILITY_MAX_ATTEMPTS);
        retryTemplate.setRetryPolicy(retryPolicy);

        HttpComponentsClientHttpRequestFactory clientRequestFactory = new HttpComponentsClientHttpRequestFactory();
        clientRequestFactory.setConnectTimeout(5 * 1000); // in ms
        RestTemplate restTemplate = new RestTemplate(clientRequestFactory);

        retryTemplate.execute(arg0 -> {
            log.info("Checking for availability of bookingservice");
            restTemplate.getForObject(bookingserviceBaseURL + "/actuator/health", String.class);
            return null;
        });
    }

    @Before
    public void setUp() {

        checkForReachabilityOfServices();

        RestAssured.baseURI = bookingserviceBaseURL;
    }

    // -------------------------------------------------------------------------------------------------------------------
    // für JSONPath siehe http://goessner.net/articles/JsonPath/ und den Tester unter https://jsonpath.curiousconcept.com/
    // -------------------------------------------------------------------------------------------------------------------
    @Test
    public void exampleTest() {
        when().
                get("/v1/customers").
                then().
                statusCode(200).
                body("lastName", hasItems("Sarstedt"));
    }
}