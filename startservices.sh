#!/usr/bin/env bash

echo Starting all services

docker network create my_docker_network
docker run --name mysql --network=my_docker_network -d -p 3306:3306 -e MYSQL_ROOT_PASSWORD="root" -e MYSQL_DATABASE="dev" mysql:8.0.12
docker run --name rabbitmq --network=my_docker_network -d -p 5672:5672 -p 15672:15672 -e RABBITMQ_DEFAULT_USER="guest" -e RABBITMQ_DEFAULT_PASS="guest" rabbitmq:3.7.7-management-alpine
docker run --name bookingservice --network=my_docker_network -d -p 8080:8080 -e SPRING_DATASOURCE_URL="jdbc:mysql://mysql:3306/dev?allowPublicKeyRetrieval=true&useSSL=false" -e SPRING_RABBITMQ_HOST="rabbitmq" registry.gitlab.com/sarstedt/adia/bookingservice:latest
./wait-for-service.sh http://localhost:8080/actuator/health

echo Finished.