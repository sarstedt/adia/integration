#!/usr/bin/env bash

echo Stopping and removing all services

docker rm -f bookingservice
docker rm -f rabbitmq
docker rm -f mysql

docker network rm my_docker_network

echo Finished.