#!/bin/bash

echo "Waiting for service $1 on endpoint $2..."
counter=0
MAXTRIES=36

while [ $counter -le $MAXTRIES ]
do
    if $(curl --output /dev/null --silent --head --fail $2); then
        echo
        echo "Service $1 has started successfully!"
        exit 0
    fi
    printf '.'
    sleep 5

    ((counter++))
done

echo
echo "WARNING: Service $1 not available after several tries! Aborting."
exit 1